<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>

<head>
    <meta charset="utf-8">
    <title>Онлайн домашка</title>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet"
          integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
            integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
            crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
            integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
            crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
            integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
            crossorigin="anonymous"></script>

    <style>
        .nav-underline .nav-link {
            padding-top: .75rem;
            padding-bottom: .75rem;
            font-size: .875rem;
            color: #6c757d;
        }

        .nav-link {
            display: block;
            padding: .5rem 1rem;
        }
    </style>

</head>

<body>
<!-- container section start -->
<div id="container" class="h-100 bg-light" style="background-image: url('resources/background.png');">

    <nav class="navbar navbar-expand-md navbar-dark bg-dark">
        <a class="navbar-brand" href="/">ДомашкаОнлайн</a>
        <form class="form-inline my-2 my-lg-0 ml-auto">
            <span class="badge badge-info">Учитель</span>
            <span class="text-light mx-2">Саковец Лилия Павловна</span>
            <img src="https://miro.medium.com/fit/c/128/128/1*vfppn5ER_ZvzQgfnc1e--w.jpeg"
                 class="rounded mr-5 d-block rounded-circle" height="40" alt="Аватар">
            <a class="btn btn-outline-light mx-2" href="/">Выход</a>
        </form>
    </nav>
    <br>
    <div class="row justify-content-md-center">
        <div class="col-10 card py-3">

            <form class="form-inline">
                <button class="btn btn-light border" type="button" data-toggle="collapse" data-target="#collapsePupils"
                        aria-expanded="true" aria-controls="collapsePupils" style="width: 250px;">
                    11 Б
                </button>
                <div class="input-group input-group-sm ml-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="basic-addon3">Ссылка доступа в класс</span>
                    </div>
                    <input type="text" class="form-control" id="basic-url" aria-describedby="basic-addon3"
                           value="http://system.by/sfHh3s">
                    <div class="input-group-append">
                        <span class="input-group-text">
                            <i class="fa fa-copy"></i>
                        </span>
                    </div>
                </div>

            </form>
            <div class="collapse show" id="collapsePupils">
                <div class="card card-body">
                    <table class="table table-sm" style="word-break: break-all;">
                        <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Фамилия Имя</th>
                            <th scope="col">
                                <input class="form-check-input" type="checkbox" value="" id="defaultCheck0">
                                &nbsp;
                            </th>
                            <th scope="col">Статус</th>
                            <th scope="col">Уровень 1</th>
                            <th scope="col">Уровень 2</th>
                            <th scope="col">Уровень 3</th>
                            <th scope="col">Уровень 4</th>
                            <th scope="col">Уровень 5</th>
                            <th scope="col">Результат</th>
                            <th scope="col">Действия</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td scope="row">1</td>
                            <td>Петрович Петя</td>
                            <td>
                                <input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
                            </td>
                            <td scope="col"><span class="badge badge-pill badge-light">Не назначено</span></td>
                            <td scope="col"></td>
                            <td scope="col"></td>
                            <td scope="col"></td>
                            <td scope="col"></td>
                            <td scope="col"></td>
                            <td scope="col"></td>
                            <td scope="col">
                                <a href="#" class="badge badge-light">
                                    <i class="fa fa-times-circle"></i>
                                </a>
                            </td>
                        </tr>
                        <tr>
                            <td scope="row">2</td>
                            <td>Иванов Ваня</td>
                            <td>
                                <input class="form-check-input" type="checkbox" value="" id="defaultCheck2" checked>
                            </td>
                            <td scope="col"><span class="badge badge-pill badge-primary">Выполняет</span></td>
                            <td scope="col"><span class="badge badge-success">Выполнен</span></td>
                            <td scope="col"><span class="badge badge-danger">Ошибка</span></td>
                            <td scope="col"><span class="badge badge-success">Выполнен</span></td>
                            <td scope="col"><span class="badge badge-secondary">Не приступал</span></td>
                            <td scope="col"><span class="badge badge-secondary">Не приступал</span></td>
                            <td scope="col">40 %</td>
                            <td scope="col">
                                <a href="#" class="badge badge-light">
                                    <i class="fa fa-times-circle"></i>
                                </a>
                                <a href="#" class="badge badge-light">Поощрить!</a>
                            </td>
                        </tr>
                        <tr>
                            <td scope="row">3</td>
                            <td>Светлова Света</td>
                            <td>
                                <input class="form-check-input" type="checkbox" value="" id="defaultCheck3" checked>
                            </td>
                            <td scope="col"><span class="badge badge-pill badge-success">Завершено</span></td>
                            <td scope="col"><span class="badge badge-success">Выполнен</span></td>
                            <td scope="col"><span class="badge badge-success">Выполнен</span></td>
                            <td scope="col"><span class="badge badge-success">Выполнен</span></td>
                            <td scope="col"><span class="badge badge-success">Выполнен</span></td>
                            <td scope="col"><span class="badge badge-success">Выполнен</span></td>
                            <td scope="col">100 %</td>
                            <td scope="col">
                                <a href="#" class="badge badge-light">
                                    <i class="fa fa-times-circle"></i>
                                </a>
                                <a href="#" class="badge badge-light">Поощрить!</a>
                            </td>
                        </tr>
                        <tr>
                            <td scope="row">4</td>
                            <td>Дмитриев Дима</td>
                            <td>
                                <input class="form-check-input" type="checkbox" value="" id="defaultCheck4">
                            </td>
                            <td scope="col"><span class="badge badge-pill badge-light">Не назначено</span></td>
                            <td scope="col"></td>
                            <td scope="col"></td>
                            <td scope="col"></td>
                            <td scope="col"></td>
                            <td scope="col"></td>
                            <td scope="col"></td>
                            <td scope="col">
                                <a href="#" class="badge badge-light">
                                    <i class="fa fa-times-circle"></i>
                                </a>
                            </td>
                        </tr>
                        <tr>
                            <td scope="row">5</td>
                            <td>Плохой Ученик</td>
                            <td>
                                <input class="form-check-input" type="checkbox" value="" id="defaultCheck5" checked>
                            </td>
                            <td scope="col"><span class="badge badge-pill badge-success">Завершено</span></td>
                            <td scope="col"><span class="badge badge-danger">Ошибка</span></td>
                            <td scope="col"><span class="badge badge-danger">Ошибка</span></td>
                            <td scope="col"><span class="badge badge-danger">Ошибка</span></td>
                            <td scope="col"><span class="badge badge-danger">Ошибка</span></td>
                            <td scope="col"><span class="badge badge-danger">Ошибка</span></td>
                            <td scope="col">0 %</td>
                            <td scope="col">
                                <a href="#" class="badge badge-light">
                                    <i class="fa fa-times-circle"></i>
                                </a>
                                <a href="#" class="badge badge-light">Поощрить!</a>
                            </td>
                        </tr>
                        <tr>
                            <td scope="row">6</td>
                            <td>Аркадий Добкин</td>
                            <td>
                                <input class="form-check-input" type="checkbox" value="" id="defaultCheck6" checked>
                            </td>
                            <td scope="col"><span class="badge badge-pill badge-secondary">Не приступал</span></td>
                            <td scope="col"><span class="badge badge-secondary">Не приступал</span></td>
                            <td scope="col"><span class="badge badge-secondary">Не приступал</span></td>
                            <td scope="col"><span class="badge badge-secondary">Не приступал</span></td>
                            <td scope="col"><span class="badge badge-secondary">Не приступал</span></td>
                            <td scope="col"><span class="badge badge-secondary">Не приступал</span></td>
                            <td scope="col">0 %</td>
                            <td scope="col">
                                <a href="#" class="badge badge-light">
                                    <i class="fa fa-times-circle"></i>
                                </a>
                                <a href="#" class="badge badge-light">Поощрить!</a>
                            </td>
                        </tr>
                        <tr>
                            <td scope="row">7</td>
                            <td>Ники Минаж</td>
                            <td>
                                <input class="form-check-input" type="checkbox" value="" id="defaultCheck7" checked>
                            </td>
                            <td scope="col"><span class="badge badge-pill badge-primary">Выполняет</span></td>
                            <td scope="col"><span class="badge badge-danger">Ошибка</span></td>
                            <td scope="col"><span class="badge badge-success">Выполнен</span></td>
                            <td scope="col"><span class="badge badge-success">Выполнен</span></td>
                            <td scope="col"><span class="badge badge-success">Выполнен</span></td>
                            <td scope="col"><span class="badge badge-secondary">Не приступал</span></td>
                            <td scope="col">60 %</td>
                            <td scope="col">
                                <a href="#" class="badge badge-light">
                                    <i class="fa fa-times-circle"></i>
                                </a>
                                <a href="#" class="badge badge-light">Поощрить!</a>
                            </td>
                        </tr>
                        <tr>
                            <td scope="row">8</td>
                            <td>Бритни Спирз</td>
                            <td></td>
                            <td scope="col"></td>
                            <td scope="col"></td>
                            <td scope="col"></td>
                            <td scope="col"></td>
                            <td scope="col"></td>
                            <td scope="col"></td>
                            <td scope="col"></td>
                            <td scope="col">
                                <a href="#" class="badge badge-light">
                                    <i class="fa fa-times-circle"></i>
                                </a>
                                <a href="#" class="badge badge-success">
                                    <i class="fa fa-check-circle"></i>
                                </a>
                            </td>
                        </tr>
                        <tr>
                            <td scope="row">9</td>
                            <td>Коля Басков</td>
                            <td>
                                <input class="form-check-input" type="checkbox" value="" id="defaultCheck9">
                            </td>
                            <td scope="col"><span class="badge badge-pill badge-light">Не назначено</span></td>
                            <td scope="col"></td>
                            <td scope="col"></td>
                            <td scope="col"></td>
                            <td scope="col"></td>
                            <td scope="col"></td>
                            <td scope="col"></td>
                            <td scope="col">
                                <a href="#" class="badge badge-light">
                                    <i class="fa fa-times-circle"></i>
                                </a>
                            </td>
                        </tr>
                        <tr style="font-weight: bold;">
                            <td colspan="4" class="text-right pr-2">Выполняемость:</td>
                            <td scope="col">40%</td>
                            <td scope="col">40%</td>
                            <td scope="col">60%</td>
                            <td scope="col">40%</td>
                            <td scope="col">20%</td>
                            <td scope="col">67 %</td>
                            <td scope="col"></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>

            <form class="form-inline mt-3">
                <button class="btn btn-light border" type="button" data-toggle="collapse"
                        data-target="#collapseExercises"
                        aria-expanded="true" aria-controls="collapseExercises" style="width: 250px;">
                    Задания для класса
                </button>
            </form>
            <div class="collapse show" id="collapseExercises">
                <div class="card card-body">
                    <table class="table table-sm table-hover">
                        <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Заголовок</th>
                            <th scope="col">Статус</th>
                            <th scope="col">Выполнено</th>
                            <th scope="col">Дедлайн</th>
                            <th scope="col">Действия</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td scope="row">1</td>
                            <td>Булева алгебра. Занятие 23</td>
                            <td><span class="badge badge-secondary">Черновик</span></td>
                            <td></td>
                            <td><span class="badge badge-danger">29.09.2019</span></td>
                            <td>
                                <a href="#" class="badge badge-light">
                                    <i class="fa fa-times-circle"></i>
                                </a>
                            </td>
                        </tr>
                        <tr class="table-active">
                            <td scope="row">2</td>
                            <td>Урок 34. Дроби</td>
                            <td><span class="badge badge-pill badge-success">Активно</span></td>
                            <td>67 %</td>
                            <td>-</td>
                            <td>
                                <a href="#" class="badge badge-light">
                                    <i class="fa fa-times-circle"></i>
                                </a>
                            </td>
                        </tr>
                        <tr>
                            <td scope="row">3</td>
                            <td>Для олимпиадников. Тригонометрия, повышенная сложность</td>
                            <td><span class="badge badge-pill badge-success">Закрыто</span></td>
                            <td>93 %</td>
                            <td>-</td>
                            <td>
                                <a href="#" class="badge badge-light">
                                    <i class="fa fa-times-circle"></i>
                                </a>
                            </td>
                        </tr>
                        <tr>
                            <td scope="row">4</td>
                            <td>Пример заголовка задания. Урок 2</td>
                            <td><span class="badge badge-secondary">Черновик</span></td>
                            <td></td>
                            <td><span class="badge badge-danger">13.04.2019</span></td>
                            <td>
                                <a href="#" class="badge badge-light">
                                    <i class="fa fa-times-circle"></i>
                                </a>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>

        </div>
    </div>

</div>
<!-- container section start -->

<script>
</script>

</body>
</html>
