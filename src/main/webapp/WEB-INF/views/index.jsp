<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>

<head>
    <meta charset="utf-8">
    <title>Онлайн домашка</title>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
            integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
            crossorigin="anonymous"></script>

    <style>
        .nav-underline .nav-link {
            padding-top: .75rem;
            padding-bottom: .75rem;
            font-size: .875rem;
            color: #6c757d;
        }

        .nav-link {
            display: block;
            padding: .5rem 1rem;
        }
    </style>

</head>

<body>
<!-- container section start -->
<div id="container" class="h-100 bg-light" style="background-image: url('resources/background.png');">

    <nav class="navbar navbar-expand-md navbar-dark bg-dark">
        <a class="navbar-brand" href="/">ДомашкаОнлайн</a>
    </nav>
    <br><br><br><br><br>
    <div class="row w-100 align-items-end">
        <div class="col-2">
        </div>
        <div class="col-3 card">
            <form class="w-75 mx-auto my-5" action="signin">
                <div class="form-group">
                    <label for="exampleInputEmail1">Email адрес</label>
                    <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Введите email">
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">Пароль</label>
                    <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Пароль">
                </div>
                <div class="form-group form-check">
                    <input type="checkbox" class="form-check-input" id="exampleCheck1">
                    <label class="form-check-label" for="exampleCheck1">Запомнить меня</label>
                </div>
                <button type="submit" class="btn btn-lg btn-info btn-block">Войти</button>
            </form>
        </div>
        <div class="col-2 text-center">
            <h4 class="text-secondary card w-50 mx-auto py-3"><strong>или</strong></h4>
            <br>
        </div>
        <div class="col-3 card">
            <form class="w-75 mx-auto my-5" action="registration">
                <div class="form-group">
                    <label for="exampleInputEmail1">Email адрес</label>
                    <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Введите email">
                    <small id="emailHelp" class="form-text text-muted">Мы никогда не передаём ваш email третьим лицам</small>
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">Пароль</label>
                    <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Пароль">
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">Повторите пароль</label>
                    <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Пароль">
                </div>
                <button type="submit" class="btn btn-lg btn-info btn-block">Зарегистрироваться</button>
            </form>
        </div>
        <div class="col-2">
        </div>
    </div>

</div>
<!-- container section start -->

<script>
</script>

</body>
</html>
