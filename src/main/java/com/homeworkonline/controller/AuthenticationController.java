package com.homeworkonline.controller;

import com.homeworkonline.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class AuthenticationController extends AbstractRestController {

    @Autowired
    UserService userService;

    @RequestMapping(value = {"/", "/index"}, method = RequestMethod.GET)
    public String indexPage(ModelMap model) {
        return "index";
    }

    @RequestMapping(value = "/signin", method = RequestMethod.GET)
    public String signinPage() {
        return "teacherDashboard";
    }

    @RequestMapping(value = "/registration", method = RequestMethod.GET)
    public String registrationPage() {
        return "teacherDashboard";
    }

    @RequestMapping(value = "/logout", method = RequestMethod.GET)
    public String logoutPage() {
        return "redirect:/";
    }

}
