package com.homeworkonline.model;

public enum UserRole {
    PUPIL(10),             // ученик
    TEACHER(20),           // учитель
    ADMIN(60);             // root user

    private int roleId;

    UserRole(int roleId) {
        this.roleId = roleId;
    }

    public int getRoleId() {
        return roleId;
    }
}
