package com.homeworkonline.model;

import java.io.Serializable;
import java.util.Date;

public class User implements Serializable {

	private Long id;
	private String login;
	private String password;
	private String email;
	private UserRole userRole;
	private String name;
	private String photoUrl;
	private Date dateOfBirth;
	private boolean active;
	private Date registeredAt;

	public User(Long id, String login, String password, String email, UserRole userRole, String name, String photoUrl, Date dateOfBirth) {
		this.id = id;
		this.login = login;
		this.password = password;
		this.email = email;
		this.userRole = userRole;
		this.name = name;
		this.photoUrl = photoUrl;
		this.dateOfBirth = dateOfBirth;
		this.active = true;
		this.registeredAt = new Date(System.currentTimeMillis());
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public UserRole getUserRole() {
		return userRole;
	}

	public void setUserRole(UserRole userRole) {
		this.userRole = userRole;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPhotoUrl() {
		return photoUrl;
	}

	public void setPhotoUrl(String photoUrl) {
		this.photoUrl = photoUrl;
	}

	public Date getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public Date getRegisteredAt() {
		return registeredAt;
	}

	public void setRegisteredAt(Date registeredAt) {
		this.registeredAt = registeredAt;
	}
}
