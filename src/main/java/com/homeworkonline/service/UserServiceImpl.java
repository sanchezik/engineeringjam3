package com.homeworkonline.service;

import com.homeworkonline.dao.UserDao;
import com.homeworkonline.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service("userService")
public class UserServiceImpl implements UserService {

	@Autowired
	private UserDao userDao;

	@Override
	public User findByLogin(String login) {
		return userDao.findByLogin(login);
	}
}
