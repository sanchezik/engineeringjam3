package com.homeworkonline.service;

import com.homeworkonline.model.User;

public interface UserService {

    User findByLogin(String login);

}
