package com.homeworkonline.dao;

import com.homeworkonline.model.User;
import org.springframework.stereotype.Repository;

@Repository("userDao")
public class UserDaoImpl extends AbstractDaoImpl implements UserDao {

    @Override
    public User findByLogin(String login) {
        if (login == null) {
            return null;
        }
        for (User user : AbstractDaoImpl.getDbUsers()) {
            if (login.equals(user.getLogin())) {
                return user;
            }
        }
        return null;
    }
}
