package com.homeworkonline.dao;

public interface AbstractDao {

	void save(Object object);

	void update(Object object);

	void delete(Object object);

}
