package com.homeworkonline.dao;

import com.homeworkonline.model.User;
import com.homeworkonline.model.UserRole;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class AbstractDaoImpl implements AbstractDao {

    private static List<User> dbUsers;

    public static List<User> getDbUsers() {
        if (dbUsers == null) {
            initUsers();
        }
        return dbUsers;
    }

    private static void initUsers() {
        dbUsers = new ArrayList<User>();
        dbUsers.add(new User(1l, "teacher1", "123", "teacher1@email.com", UserRole.TEACHER, "Лилия Павловна Саковец", "https://miro.medium.com/fit/c/128/128/1*vfppn5ER_ZvzQgfnc1e--w.jpeg", new Date(System.currentTimeMillis())));
        dbUsers.add(new User(2l, "teacher2", "123", "teacher2@email.com", UserRole.TEACHER, "Сергей Васильевич Кривовяз", "https://i1.rgstatic.net/ii/profile.image/474323753410561-1490099003105_Q128/Wing_Ho_Man.jpg", new Date(System.currentTimeMillis())));
        dbUsers.add(new User(3l, "pupil1", "123", "pupil1@email.com", UserRole.PUPIL, "Ваня Иванов", "https://sun9-16.userapi.com/c852036/v852036764/b2f2d/MOo4r2xK6tc.jpg", new Date(System.currentTimeMillis())));
        dbUsers.add(new User(4l, "pupil2", "123", "pupil2@email.com", UserRole.PUPIL, "Лена Петрова", "https://cdn17.picsart.com/77637599963.jpeg?c256x256", new Date(System.currentTimeMillis())));
        dbUsers.add(new User(5l, "pupil3", "123", "pupil3@email.com", UserRole.PUPIL, "Петя Добкин", "https://resources.finalsite.net/images/f_auto,q_auto,t_image_size_1/v1532886977/regis/f9oeemqfugqev2bcclpi/homems.jpg", new Date(System.currentTimeMillis())));
        dbUsers.add(new User(6l, "pupil4", "123", "pupil4@email.com", UserRole.PUPIL, "Саша Сидоров", "https://i1.wp.com/attirepin.com/wp-content/uploads/2018/09/Trendy-and-Cute-Toddler-Boy-Haircuts-Your-Kids-Will-Lovel-82.jpg?w=256&h=256&crop=1&ssl=1", new Date(System.currentTimeMillis())));
        dbUsers.add(new User(7l, "pupil5", "123", "pupil5@email.com", UserRole.PUPIL, "Даша Васечкина", "https://pbs.twimg.com/profile_images/422167924103606272/tyqPRL88.jpeg", new Date(System.currentTimeMillis())));
    }

    @Override
    public void save(Object object) {
        if (object instanceof User) {
            dbUsers.add((User) object);
        }
    }

    @Override
    public void update(Object object) {
        if (object instanceof User) {
            List<User> newUsers = new ArrayList<User>();
            for (User user : dbUsers) {
                if (user.getId() == ((User) object).getId()) {
                    newUsers.add((User) object);
                } else {
                    newUsers.add(user);
                }
            }
            dbUsers = newUsers;
        }
    }

    @Override
    public void delete(Object object) {
        if (object instanceof User) {
            ((User) object).setActive(false);
            List<User> newUsers = new ArrayList<User>();
            for (User user : dbUsers) {
                if (user.getId() == ((User) object).getId()) {
                    newUsers.add((User) object);
                } else {
                    newUsers.add(user);
                }
            }
            dbUsers = newUsers;
        }
    }
}
