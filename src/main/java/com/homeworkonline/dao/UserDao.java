package com.homeworkonline.dao;

import com.homeworkonline.model.User;

public interface UserDao extends AbstractDao {

	User findByLogin(String login);

}
